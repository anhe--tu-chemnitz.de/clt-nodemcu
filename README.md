CLT-Workshop: Internet der Dinge mit dem NodeMCU
================================================

Dieses GIT-Archiv enthält die Dokumentation und die Codebeispiele des CLT-Workshop.


Quickstart
----------

Für die Nutzung des Lua-Interpreters über die seriellen Konsole des ESP8266-Modules kannst Du `minicom` oder `screen` verwenden.

Wie Du `udev` konfigurierst, um als angemeldeter Benutzer Zugriff auf die serielle Schnittstelle zu erhalten erfährst Du in der beiliegenden Dokumentation.
Ggf. musst Du den Namen der seriellen Schnittstelle anpassen. 

```bash
minicom -D /dev/ttyUSB0 -b 115200 -o
```

Beispiel hochladen und ausführen
--------------------------------

Mit dem Werkzeug `nodemcu-uploader` kannst Du die Codebespiel auf das Filesystem des ESP8266-Modules hochladen.
(Dazu solltest Du vorher die Anwendungen `minicom` oder `screen`, welche die serielle Schnittstelle des ESP8266-Modules geöffnet haben beenden.)

Die Integration von  `nodemcu-uploader` in `minicom` ist ebenfalls in der Dokumentation beschrieben.

```bash
/usr/bin/nodemcu-uploader --port /dev/ttyUSB0 upload blink.lua
```

Für das Starten der Skripts gibt es verschiedenen Möglichkeiten.
Ein Variante bietet die Lua-Funktion `dofile` (Zugriff auf Lua-Interpreter über serielle Schnittstelle erforderlich).

```lua
dofile("blink.lua") 
```




