-- siehe: http://famory-toure.blogspot.de/2015/04/esp8266-animation-ruban-de-leds-ws2812b.html

ws2812.init()

rainbow_pattern = string.char(  
   0x00, 0x00, 0xff,  
   0x00, 0xaa, 0xdd,  
   0x00, 0xdd, 0xaa,  
   0x00, 0xff, 0x00,  
   0xaa, 0xdd, 0x00,  
   0xdd, 0xaa, 0x00,  
   0xff, 0x00, 0x00,  
   0xdd, 0x00, 0xaa,  
   0xaa, 0x00, 0xdd  
 )

rainbow_count = string.len(rainbow_pattern)  
led_count = 20  
rainbows = (led_count * 3) / rainbow_count  

partial_rainbows = rainbows % 1  
pattern = string.rep(rainbow_pattern, rainbows - partial_rainbows)..string.sub(rainbow_pattern, 1, partial_rainbows * rainbow_count)  

function rotate(s, n, delay)  
  for i = 0, n do  
   ws2812.write(s)  
   beg = string.sub(s, 0, 3)  
   s = string.sub(s, 3, string.len(s) - 3)..beg  
   tmr.wdclr()  
   tmr.delay(delay * 1000)  
  end  
end  

for i=0,100 do  
  rotate(pattern, 5, 40)  
  tmr.wdclr()  
end
