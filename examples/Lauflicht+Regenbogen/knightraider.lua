ws2812.init()

function k2000(n, delay)  
 s = string.char(23, 63, 23):rep(5)..string.char(0, 0, 0):rep(n - 5)  
 for i = 0, n - 6 do  
  s = string.char(0, 0, 0)..string.sub(s, 0, string.len(s)-3)  
  ws2812.write(s)  
  tmr.wdclr()  
  tmr.delay(delay * 1000)  
 end  
 for i = 0, n - 6 do  
  s = string.sub(s, 4, string.len(s))..string.char(0, 0, 0)  
  ws2812.write(s)  
  tmr.wdclr()  
  tmr.delay(delay * 1000)  
 end  
 end  
   
 for i = 0, 20 do  
   k2000(20, 25)  
 end  
