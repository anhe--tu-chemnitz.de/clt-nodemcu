-- Regenbogenlicht, 20 Farben/s
-- CLT 2017

ws2812.init()
TIME       = 50  -- 0.05 second,  20 Hz
LEDS       = 20  -- Anzahl LED
BRIGHT     = 0.2 -- Helligkeit

i = 0

green = {0, 61, 127, 193, 255, 255, 255, 255, 255, 255, 255, 255, 255, 193, 127, 61, 0, 0, 0, 0, 0, 0, 0, 0, 0}
red   = {255, 255, 255, 255, 255, 183, 127, 61, 0, 0, 0, 0, 0, 0, 0, 0, 0, 61, 127, 193, 255, 255, 255, 255, 255}
blue  = {0, 0, 0, 0, 0, 0, 0, 0, 0, 61, 127, 193, 255, 255, 255, 255, 255, 255, 255, 255, 255, 193, 127, 61, 0}

print(table.getn(green))


function show()
  i = (i % table.getn(green))+1
  print(i, red[i], green[i], blue[i])
  COLOR = string.char(BRIGHT * red[i], BRIGHT * green[i], BRIGHT * blue[i]) 
  ws2812.write(COLOR:rep(LEDS))
end


tmr.alarm(0, TIME, 1, function() show() end )

print("One WS2812 connected to Pin D4 will changes its color repeatedly")
print("Stop this by tmr.stop(0)")
show()
