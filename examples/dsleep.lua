LED=4
gpio.mode(LED, gpio.OUTPUT)

for x=1,5 do

  for i=1,3 do
    gpio.write(LED, gpio.LOW)
    tmr.delay(500000)   -- wait 500,000 us = 0,5 second
    gpio.write(LED, gpio.HIGH)
    tmr.delay(500000)   -- wait 500,000 us = 0,5 second
  end

  print("Enter deep sleep ...")
  node.dsleep(10000000) -- 10 sek
  print("... wieder aufgewacht")

end

