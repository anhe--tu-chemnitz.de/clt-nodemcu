
-- init adc
if adc.force_init_mode(adc.INIT_VDD33)
then
  node.restart()
  return -- 
end

-- draw battery symbol
function battery(l)
    local symbol = "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"45\" height=\"59\">\n"
    symbol = symbol .. "  <path\n"
    symbol = symbol .. "    d=\"m 15,10 0.01,-2.0757899 c 0,-1.0710598 0.86227,-1.9333298 1.93333,-1.9333298 l 11.13334,0 c 1.07106,0 1.93333,0.86227 1.93333,1.9333298 l -0.005,2.1661199 m -22.0666595,-0.0995 29.1333395,0 c 1.07106,0 1.93333,0.8622699 1.93333,1.9333299 l 0,39.13334 c 0,1.071 -0.86227,1.9333 -1.93333,1.9333 l -29.1333395,0 c -1.0710648,0 -1.9333298,-0.8623 -1.9333298,-1.9333 l 0,-39.13334 c 0,-1.07106 0.862265,-1.9333299 1.9333298,-1.9333299 z\"\n"
    symbol = symbol .. "    style=\"fill:none;stroke:#2290ac;stroke-width:2\"\n"
    symbol = symbol .. "    id=\"batt-0\" />\n"
    if l >= 0.25 then
        symbol = symbol .. "  <rect rx=\"2\" y=\"42\" x=\"8\" height=\"9\" width=\"29\" id=\"batt-1\" style=\"fill:#00ccff;stroke:none\" />\n"
    end
    if l >= 0.5 then
        symbol = symbol .. "  <use xlink:href=\"#batt-1\" y=\"-10\" x=\"0\" />\n"
    end
    if l >= 0.75 then
        symbol = symbol .. "  <use xlink:href=\"#batt-1\" y=\"-20\" x=\"0\" />\n"
    end
    if l >= 1 then
        symbol = symbol .. "  <use xlink:href=\"#batt-1\" y=\"-30\" x=\"0\" />\n"
    end
    symbol = symbol .. "</svg>\n"
    return symbol
end

-- web interface
srv = net.createServer(net.TCP)
srv:listen(80, function(conn)
    conn:on("receive", function(conn, payload)
        print(payload)

    -- HTML-Ausgabe
    local buf = ""
    buf = buf.."HTTP/1.1 200 OK\n\n"
    buf = buf.."<!DOCTYPE HTML>\n"
    buf = buf.."<html>\n"
    buf = buf.."<head><meta  content=\"text/html; charset=utf-8\">\n"
    buf = buf.."<title>Batterie Status</title></head>\n"
    buf = buf.."<body>\n"
    buf = buf.."<h2>Batterie Status</h2>\n"
    local voltage = adc.readvdd33(0)/1000
    buf = buf.."<p>Batteriespannung: "..voltage.." V<br/>\n"
    buf = buf..battery(voltage-2).."<p>\n"
    buf = buf.."</body></html>\n"
    conn:send(buf)
    end)
    conn:on("sent", function(conn) conn:close() end)
end)

