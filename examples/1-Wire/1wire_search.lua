-- 1-Wire bus an Pin
pin = 6
ow.setup(pin)
ow.reset_search(pin)
-- search for 1-wire devices
repeat
    addr = ow.search(pin)
    if addr ~=nil then
        -- format address
        local str = {}
            for i=1,8 do
            table.insert(str, string.format("%02x", addr:byte(i)))
        end
        -- crc vergleichen
        if ow.crc8(string.sub(addr,1,7)) == addr:byte(8) then
                print(table.concat(str,':'))
        else
            print("CRC-Fehler")
        end
    end
until (addr == nil)


