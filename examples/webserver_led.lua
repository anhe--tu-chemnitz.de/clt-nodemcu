-- integrierte, blaue LED am GPIO4
LED=4
gpio.mode(LED, gpio.OUTPUT)
-- Webserver
srv=net.createServer(net.TCP)
srv:listen(80, function(conn)
  conn:on("receive", function(conn,payload)
    -- empfangene payload daten
    print(payload)
    function switch_led()
      -- liefert payload ab gefundener position bis ende
      value = string.sub(payload, pos_array[2]+1, #payload)
      print('### DEBUG ### Switch '..value)
      if value == "LED+on"  then
        gpio.write(LED, gpio.LOW)
      elseif value == "LED+off"  then
        gpio.write(LED, gpio.HIGH)
      end
    end
    -- suche start/end-position von "my_action=" in payload als array
    pos_array = {string.find(payload,"my_action=")}
    -- end-position gefunden
    if pos_array[2] ~= nil then
      switch_led()
    end
    -- HTML-Ausgabe
    local buf = ''
    buf = buf..'HTTP/1.1 200 OK\n\n'
    buf = buf..'<!DOCTYPE HTML>\n'
    buf = buf..'<html>\n'
    buf = buf..'<head><meta  content="text/html; charset=utf-8">\n'
    buf = buf..'<title>ESP8266 Webserver Demo</title></head>\n'
    buf = buf..'<body><h3>ESP8266 Webserver Demo</h3>\n'
    buf = buf..'<form action="" method="POST">\n'
    buf = buf..'<input type="submit" name="my_action" value="LED on">\n'
    buf = buf..'<input type="submit" name="my_action" value="LED off">\n'
    buf = buf..'</body></html>\n'
    conn:send(buf)
  end)
  conn:on("sent", function(conn) conn:close() end)
end)
