-- weather station app

-- init adc
if adc.force_init_mode(adc.INIT_VDD33)
then
  node.restart()
  return -- don't bother continuing, the restart is scheduled
end


-- show state, blinking led
GPIO = 4
gpio.mode(GPIO, gpio.OUTPUT)
function blink_state(count, delay)
  print('Call blink_state', count, delay)
  C=1
  blink = tmr.create()
  tmr.alarm(blink, delay, tmr.ALARM_AUTO, function()
    gpio.write(GPIO, (C%2==0) and gpio.HIGH or gpio.LOW)
    C=C+1
    if C > count then
      tmr.unregister(blink)
      gpio.write(GPIO, gpio.HIGH)
    end
  end)
end


-- ntp time
TZ=60*60*2 
sntp.sync({"pool.ntp.org"},
	function(sec, usec, server, info)
		print('NTP sync', sec, usec, server)
	end, 
	function()
		print('NTP sync failed!')
  	end,
	1)

-- measure and save data into a ring buffer of 24 slots
data = {}
sda,scl=1,2
i2c.setup(0,sda,scl,i2c.SLOW)
bme=bme280.setup()
function measure()
	local now = rtctime.epoch2cal(rtctime.get()+TZ)
	datestring = string.format("%04d/%02d/%02d %02d:%02d:%02d", now["year"], now["mon"], now["day"], now["hour"], now["min"], now["sec"])
	table.insert(data, 1, {datestring, bme280.temp(), bme280.baro(), bme280.humi()}) 
	table.remove(data, 25)
	-- change the timer interval to 1 hour
	tmr.interval(measure_timer, 1000*60*30)
end


if bme==2 then
  blink_state(10,500)
  -- register measure timer and start after 10 secs
  measure_timer = tmr.create()
  tmr.alarm(measure_timer, 1000*10, tmr.ALARM_AUTO, measure)
else
  blink_state(10,150)
  table.insert(data, 1, {"Error init BME280 Sensor", 0, 0, 0})
end

-- web interface
srv = net.createServer(net.TCP)
srv:listen(80, function(conn)
    conn:on("receive", function(conn, payload)
        print(payload)

	-- HTML-Ausgabe
	local buf = ""
	buf = buf.."HTTP/1.1 200 OK\n\n"
	buf = buf.."<!DOCTYPE HTML>\n"
	buf = buf.."<html>\n"
	buf = buf.."<head><meta  content=\"text/html; charset=utf-8\">\n"
	buf = buf.."<title>ESP8266 Weather Station</title></head>\n"
	buf = buf.."<body>\n"
	buf = buf.."<h2>ESP8266 Weather Station</h2>\n"
	buf = buf.."<p>Batterie: "..(adc.readvdd33(0)/1000).." V</p>\n"
	buf = buf.."<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\">\n"
	buf = buf.."<tr><th>Datum/Uhrzeit</th><th>Temperatur</th><th>Luftdruck</th><th>Feuchte</th></tr>\n"
	for k,v in pairs(data) do
		--print (k, v[1], v[2]/100, v[3]/1000, v[4]/1000)
		buf = buf.."<tr><td>"..v[1].."</td><td>"..(v[2]/100).." &deg;C</td><td>"..(v[3]/1000).." hPa</td><td>"..(v[4]/1000).." %</td></tr>\n"
	end
	buf = buf.."</table>\n"
	buf = buf.."</body></html>\n"
	conn:send(buf)
    end)
    conn:on("sent", function(conn) conn:close() end)
end)


