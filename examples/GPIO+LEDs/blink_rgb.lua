-- Blink RGB
-- rot  - D0
-- grün - D1
-- blau - D2
gpio.mode(0, gpio.OUTPUT)
gpio.mode(1, gpio.OUTPUT)
gpio.mode(2, gpio.OUTPUT)
while 1 do
  gpio.write(0, gpio.HIGH)
  tmr.delay(1000000)   -- wait 1,000,000 us = 1 second
  gpio.write(0, gpio.LOW)
  gpio.write(1, gpio.HIGH)
  tmr.delay(1000000)   -- wait 1,000,000 us = 1 second
  gpio.write(1, gpio.LOW)
  gpio.write(2, gpio.HIGH)
  tmr.delay(1000000)   -- wait 1,000,000 us = 1 second
  gpio.write(2, gpio.LOW)
end
