-- Blink
gpio.mode(4, gpio.OUTPUT)
while 1 do
  gpio.write(4, gpio.HIGH)
  tmr.delay(1000000)   -- wait 1,000,000 us = 1 second
  gpio.write(4, gpio.LOW)
  tmr.delay(1000000)   -- wait 1,000,000 us = 1 second
end
