-- blink mit tmr.alarm()-event
GPIO = 4
STATUS = true
gpio.mode(GPIO, gpio.OUTPUT)
-- blink funktion
function blink()
  if STATUS then
    gpio.write(GPIO, gpio.LOW)
  else
    gpio.write(GPIO, gpio.HIGH)
  end
  STATUS = not STATUS
end
-- timer
myblink = tmr.create()
tmr.alarm(myblink, 1000, tmr.ALARM_AUTO, blink)
-- stop blink
--tmr.unregister(myblink)
